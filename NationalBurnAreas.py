import arcpy
import os
import numpy as np
arcpy.env.overwriteOutput = True

working_dir = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas"
arcpy.env.workspace = os.path.join(working_dir, 'NationalBurnsGeoD.gdb' )
arcpy.CheckOutExtension("Spatial")

# for file in os.listdir("C:\\Users\\bhegman.stu\\Documents\\FCOR599\\599Project\\NationalBurnAreas"):
#     if file.endswith(".shp"):
#         arcpy.analysis.Clip(os.path.join(r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas", file), "GeneralClip",
#                             "NationalBurn" + file[5:9] + "GeneralClip",
#                             None)
#         currentFile = "NationalBurn" + file[5:9] + "GeneralClip"
#         arcpy.management.Project(currentFile,
#                                  currentFile[0:16] + "UTM",
#                                  "PROJCS['UTM_Zone_10N',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-123.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]",
#                                  "WGS_1984_(ITRF00)_To_NAD_1983",
#                                  "PROJCS['NAD_1983_Lambert_Conformal_Conic',GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Lambert_Conformal_Conic'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-95.0],PARAMETER['Standard_Parallel_1',49.0],PARAMETER['Standard_Parallel_2',77.0],PARAMETER['Latitude_Of_Origin',49.0],UNIT['Meter',1.0]]",
#                                  "NO_PRESERVE_SHAPE", None, "NO_VERTICAL")
#         RasFile = currentFile[0:16] + "UTM"
#         arcpy.conversion.FeatureToRaster(RasFile, "YEAR",
#                                          "Burn" + RasFile[12:16] + "Ras",
#                                          r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NDVI_NBR_TIFS\CaribooLandsat2000_NBR.tif")
#
# for file in arcpy.ListRasters():
#     if str(file[0]) == "B":
#         out_raster = arcpy.sa.Times("CaribooShapeConv", file)
#         out_raster.save("Final" + file)
#
# arr = arcpy.RasterToNumPyArray("FinalBurn2018Ras", nodata_to_value = -1)

arcpy.CheckInExtension("Spatial")