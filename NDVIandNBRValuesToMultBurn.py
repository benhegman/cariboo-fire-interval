import arcpy
import os
from itertools import combinations

arcpy.env.overwriteOutput = True
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"

rasterRows = arcpy.SearchCursor("BurnOverlapsFinal_Copy")
rasterFields = arcpy.ListFields("BurnOverlapsFinal_Copy")

IntervalDict = {}
for row in rasterRows:
    BurnYearList = []
    for field in rasterFields:
        FieldName = field.name
        value = row.getValue(FieldName)
        if value > 0 and FieldName.startswith("Reclass"):
            BurnYearList.append(value)
    IntervalDict[row] = BurnYearList

IntervalDictFew = {key:value for key, value in IntervalDict.items() if len(value) > 2}  # more than 2 burns
IntervalDict = {key:value for key, value in IntervalDict.items() if len(value) == 2}  # only burned twice

DoubleBurn = {}
for key, value in IntervalDict.items():
    DoubleBurn[key] = value[1]-value[0]

MultiBurn = {}
for key, value in IntervalDictFew.items():
    BurnIntList = [y-x for x, y in combinations(value, 2)]
    MultiBurn[key] = [BurnIntList]



#  out_raster = arcpy.sa.Reclassify("BurnOverlapsFinal_Copy", "Value", "1 0;2 0;3 0;4 7;5 0;6 3;7 0;8 0;9 0;10 9;11
#  0;12 1;13 0;14 0;15 0;16 0;17 0;18 0;19 18;20 0;21 0;22 0;23 3;24 17;25 0;26 6;27 4;28 6;29 5;30 1;31 2;32 2;33 33;34
#  5;35 35;36 3;37 1;38 5;39 0;40 1;41 4;42 2;43 3;44 2;45 0;46 46;47 8;48 3;49 49;50 50;51 51;52 10;53 8;54 4;55 4;56
#  56;57 57;58 58;59 59;60 2;61 5;62 11;63 1;64 1;65 10;66 6;67 6;68 5;69 8;70 3;71 4", "DATA")
#  out_raster.save(r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb\Reclass_BurnIntervals")