import arcpy
import os

arcpy.CheckOutExtension("Spatial")  # check out the needed extension
arcpy.env.overwriteOutput = True

arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
inputEnv = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
outputEnv = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NDVI_NBR_TIFS"
toTiff = []
for dirpath, dirnames, filenames in arcpy.da.Walk(inputEnv,
                                                      datatype="RasterDataset",
                                                    type="ANY"):
    for fname in filenames:
        if not "NBR" or "NDVI" in str(fname):
            toTiff.append(str(fname))
        else:
            continue

for i in toTiff[14:]:
    arcpy.management.CopyRaster(i,
                                r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NDVI_NBR_TIFS"+ "\\" + i + ".tif",
                                None, None, -3.402823e+38, "NONE", "NONE", None, "NONE", "NONE", "TIFF", "NONE")


arcpy.CheckInExtension("Spatial")