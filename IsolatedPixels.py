import arcpy
import numpy as np
import matplotlib.pyplot as plt
import psutil
import gc

arcpy.env.overwriteOutput = True
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"

binaryBurnRasters = [a for a in arcpy.ListRasters() if a.startswith("FinalBurn")]
binaryBurnArrayList = [arcpy.RasterToNumPyArray(x) for x in binaryBurnRasters[0:18]]

#binary raster lists
binFirstQuarter = []
# binSecondQuarter = []
# binThirdQuarter = []
# binFourthQuarter = []

binSplitList = [np.vsplit(i, 4) for i in binaryBurnArrayList]
for splitYear in binSplitList:
    binFirstQuarter.append(splitYear[0])
    # binSecondQuarter.append(splitYear[1])
    # binThirdQuarter.append(splitYear[2])
    # binFourthQuarter.append(splitYear[3])

binTestQuarter = np.dstack(binFirstQuarter)

burnAddedArray = np.sum(binTestQuarter, axis=2)
onlyTwoBurn = np.where((burnAddedArray > 2) | (burnAddedArray<2), 0, burnAddedArray)


# rasters with years
burnRasters = [a for a in arcpy.ListRasters() if a.startswith("Reclass")]
burnArrayList = [arcpy.RasterToNumPyArray(x) for x in burnRasters[0:18]]


splitList = [np.vsplit(i, 4) for i in burnArrayList]

firstQuarter = []
# secondQuarter = []
# thirdQuarter = []
# fourthQuarter = []

for splitYear in splitList:
    firstQuarter.append(splitYear[0])
    # secondQuarter.append(splitYear[1])
    # thirdQuarter.append(splitYear[2])
    # fourthQuarter.append(splitYear[3])

firstBurnYear = np.zeros((2412, 12643), dtype="uint16")
secondBurnYear = np.zeros((2412, 12643), dtype="uint16")

for i in firstQuarter:
    i = np.where(onlyTwoBurn == 2, i, 0)
    firstBurnYear = np.where((onlyTwoBurn>0) & ())

for i in reversed(firstQuarter):
    np.put(firstBurnYear, onlyTwoBurn, i)

testStackBurnYears = np.dstack((firstBurnYear, secondBurnYear))
testQuarter = np.dstack(firstQuarter)
np.put(secondBurnYear, onlyTwoBurn, testQuarter[:, :, 0:18].max())


# np arrays for ndvi
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
NDVIList = [a for a in arcpy.ListRasters() if a.endswith("NDVI")]
NDVIList.sort(key=lambda year: year[14:18])

landsatListNDVI = [arcpy.RasterToNumPyArray(a) for a in NDVIList]
for x in landsatListNDVI:
    x = x.astype("float32")
NDVIsplitList = [np.vsplit(i, 4) for i in landsatListNDVI]

NDVIFirstQuarter = []
# NDVIsecondQuarter = []
# NDVIthirdQuarter = []
# NDVIfourthQuarter = []

for splitYear in NDVIsplitList:
    NDVIFirstQuarter.append(splitYear[0])
    # NDVIsecondQuarter.append(splitYear[1])
    # NDVIthirdQuarter.append(splitYear[2])
    # NDVIfourthQuarter.append(splitYear[3])

for i in NDVIFirstQuarter:
    i = np.where(onlyTwoBurn == 2, i, 0)

NDVITest = np.dstack(NDVIFirstQuarter)

burnAndNDVI = np.dstack((NDVITest, testQuarter, onlyTwoBurn))

test = np.zeros((5,5), "uint8")
test1 = np.zeros((5,6), "uint8")

test2 = test1[:, 0:5]