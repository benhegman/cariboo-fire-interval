import arcpy
import os
import numpy as np
import matplotlib.pyplot as plt
import psutil
import gc
print(psutil.virtual_memory())
arcpy.env.overwriteOutput = True
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"

# rasters with years
burnRasters = [a for a in arcpy.ListRasters() if a.startswith("Reclass")]
burnArrayList = [arcpy.RasterToNumPyArray(x) for x in burnRasters[0:18]]
for x in burnArrayList:
    x = x.astype("float32")

#binary versions of those rasters
binaryBurnRasters = [a for a in arcpy.ListRasters() if a.startswith("FinalBurn")]
binaryBurnArrayList = [arcpy.RasterToNumPyArray(x) for x in binaryBurnRasters[0:18]]
for x in binaryBurnArrayList:
    x = x.astype("float32")
#binary raster lists
binFirstQuarter = []
binSecondQuarter = []
binThirdQuarter = []
binFourthQuarter = []

binSplitList = [np.vsplit(i, 4) for i in binaryBurnArrayList]
for splitYear in binSplitList:
    binFirstQuarter.append(splitYear[0])
    binSecondQuarter.append(splitYear[1])
    binThirdQuarter.append(splitYear[2])
    binFourthQuarter.append(splitYear[3])

# list with quarter np arrays for binary files
binQuarterList = [binFirstQuarter, binSecondQuarter, binThirdQuarter, binFourthQuarter]
binTestQuarter = np.dstack(binFirstQuarter)
# next two lines find only the pixels that burned twice
burnAddedArray = np.sum(binTestQuarter, axis=2)
onlyTwoBurn = np.where((burnAddedArray > 2) | (burnAddedArray<2), 0, burnAddedArray)

splitList = [np.vsplit(i, 4) for i in burnArrayList]

firstQuarter = []
secondQuarter = []
thirdQuarter = []
fourthQuarter = []

for splitYear in splitList:
    firstQuarter.append(splitYear[0])
    secondQuarter.append(splitYear[1])
    thirdQuarter.append(splitYear[2])
    fourthQuarter.append(splitYear[3])

quarterList = [firstQuarter, secondQuarter, thirdQuarter, fourthQuarter]

testQuarter = np.dstack(firstQuarter)

# np arrays for ndvi
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
NDVIList = [a for a in arcpy.ListRasters() if a.endswith("NDVI")]
NDVIList.sort(key=lambda year: year[14:18])

landsatListNDVI = [arcpy.RasterToNumPyArray(a) for a in NDVIList]
for x in landsatListNDVI:
    x = x.astype("float32")
NDVIsplitList = [np.vsplit(i, 4) for i in landsatListNDVI]

NDVIfirstQuarter = []
NDVIsecondQuarter = []
NDVIthirdQuarter = []
NDVIfourthQuarter = []

for splitYear in NDVIsplitList:
    NDVIfirstQuarter.append(splitYear[0])
    NDVIsecondQuarter.append(splitYear[1])
    NDVIthirdQuarter.append(splitYear[2])
    NDVIfourthQuarter.append(splitYear[3])

quarterList = [NDVIfirstQuarter, NDVIsecondQuarter, NDVIthirdQuarter, NDVIfourthQuarter]
NDVITest = np.dstack(NDVIfirstQuarter)

burnAndNDVI = np.dstack((NDVITest, testQuarter, onlyTwoBurn))

#landsatListNBR = [arcpy.RasterToNumPyArray(a) for a in arcpy.ListRasters() if a.endswith("NBR")]
#NBRStack = np.dstack(landsatListNBR)


indices = [burnAndNDVI[]]
isolatedPixelValues = burnAndNDVI[onlyTwoBurn]






