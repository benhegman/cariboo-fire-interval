""""
Ben Hegman
ben.hegman@gmail.com
11/18/2019
Extracting NDVI
"""

import arcpy
from arcpy.sa import *
import os
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\599Project.gdb"
inDirect = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
arcpy.env.overwriteOutput = True
arcpy.CheckOutExtension("Spatial")

LandsatFiles = []
for dirpath, dirnames, filenames in arcpy.da.Walk(inDirect,
                                                      datatype="RasterDataset",
                                                    type="ANY"):
    continue
for fname in filenames:
    if str(fname[0]) == "C":
        LandsatFiles.append(str(fname))

for i in LandsatFiles:
    arcpy.management.Clip(i,
                          "313237.3234 5636256.5558 692498.4877 5925653.6654", "Cariboo"+ i,
                          r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\599Project.gdb\CaribooRegion_Transverse", 65535, "ClippingGeometry", "NO_MAINTAIN_EXTENT")


for i in LandsatFiles:
    Band3 = os.path.join(inDirect, i, "Band_3")
    Band3F = Float(Band3)
    NIR = os.path.join(inDirect, i, "Band_4")
    NIRF = Float(NIR)
    SWIR = os.path.join(inDirect, i, "Band_6")
    SWIRF = Float(SWIR)
    NDVI = arcpy.sa.Divide(arcpy.sa.Minus(NIRF, Band3F), (arcpy.sa.Plus(NIRF, Band3F)))
    NDVI.save(i + '_NDVI')
    NBR = arcpy.sa.Divide(arcpy.sa.Minus(NIRF, SWIRF), arcpy.sa.Plus(NIRF, SWIRF))
    NBR.save(i+'_NBR')


for i in LandsatFiles[44:45]:
    NIR = os.path.join(inDirect, i, "Band_4")
    NIRF = Float(NIR)
    SWIR = os.path.join(inDirect, i, "Band_6")
    SWIRF = Float(SWIR)
    NBR = arcpy.sa.Divide(arcpy.sa.Minus(NIRF, SWIRF), arcpy.sa.Plus(NIRF, SWIRF))
    NBR.save('CaribooLandsat2001_NBR')


arcpy.CheckInExtension("Spatial")