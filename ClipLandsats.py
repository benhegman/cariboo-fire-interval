""""
Ben Hegman
ben.hegman@gmail.com
11/18/2019
Clipping Rasters
"""
import arcpy
import os
import sys

arcpy.CheckOutExtension("Spatial")  # check out the needed extension
arcpy.env.overwriteOutput = True

arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\Data"
inputEnv = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\Data"
LandsatList = []
for dirpath, dirnames, filenames in arcpy.da.Walk(inputEnv,
                                                      datatype="RasterDataset",
                                                    type="ANY"):
    for fname in filenames:
        LandsatList.append(fname)
for i in LandsatList:
    arcpy.management.Clip(str(i),
                            "313237.3234 5636256.5558 692498.4877 5925653.6654",
                            os.path.join(r"C:\Users\bhegman.stu\Documents\FCOR599\599Project","LandsatClipped.gdb", "CaribooLandsat") + str(i[12:16]),
                            r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\599Project.gdb\CaribooRegion_Transverse", 65536, "ClippingGeometry", "NO_MAINTAIN_EXTENT")
for i in LandsatList:
    arcpy.management.Clip(str(i), "313237.3234 5636256.5558 692498.4877 5925653.6654", os.path.join(r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb", "Clip_") + str(i),
                          r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\599Project.gdb\CaribooRegion_Transverse", 65536, "NONE", "NO_MAINTAIN_EXTENT")

arcpy.CheckInExtension("Spatial")