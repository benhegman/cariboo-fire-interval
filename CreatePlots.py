import matplotlib
import matplotlib.pyplot as plt
import pickle
from statistics import mean
from scipy import stats
import numpy as np


# load in lists created from "NonSplit.py".
# desiredLists = [intervalBurnYear, NDVIBeforeFirstBurn, NDVIBeforeSecondBurn, zScores, stDevArray] for indexing
fname = r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\lists599Proj.pkl'
with open(fname, "rb") as src:
    desiredListsNDVI = pickle.load(src)
# desiredLists2 = [stDevArrayNBR, NBRBeforeFirstBurn, NBRBeforeSecondBurn, NBRDict, NBRDiff, zScoresNBR] for indexing
fname = r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\lists599Proj2.pkl'
with open(fname, "rb") as src:
    desiredListsNBR = pickle.load(src)

intervalBurnYear = np.asarray(desiredListsNDVI[0])

# Histogram of time between fires
histoCountList = [desiredListsNDVI[0].count(x) for x in range(1, 18)]
meanList = round(mean(desiredListsNDVI[0]), 1)

#bar chart - fire to fire
plt.bar(range(1, 18), histoCountList)
plt.title("Fire-To-Fire Disturbances")
plt.xlabel("Years Between Disturbances")
plt.xticks(range(1, 18), range(1, 18))
plt.ylabel("Frequency")
# centers the label above the bar depending on how big the number is
for i in range(17):
    if histoCountList[i] > 9999:
        plt.text(x = range(1, 18)[i] - .5, y = range(1, 18)[i]+ histoCountList[i] + 1000, s = histoCountList[i], size = 6)
    elif histoCountList[i] > 999:
        plt.text(x=range(1, 18)[i] - .4, y=range(1, 18)[i] + histoCountList[i] + 1000, s=histoCountList[i], size=6)
    elif histoCountList[i] > 99:
        plt.text(x=range(1, 18)[i] - .3, y=range(1, 18)[i] + histoCountList[i] + 1000, s=histoCountList[i], size=6)
    elif histoCountList[i] > 9:
        plt.text(x=range(1, 18)[i] - .2, y=range(1, 18)[i] + histoCountList[i] + 1000, s=histoCountList[i], size=6)
    else:
        plt.text(x=range(1, 18)[i] - .1, y=range(1, 18)[i] + histoCountList[i] + 1000, s=histoCountList[i], size=6)
plt.axvline(meanList, color='red', linewidth=2, alpha = .5)
plt.show()
plt.savefig(r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\fireToFireCount')

#for NDVI
NDVIDiff = np.asarray(desiredListsNDVI[1])-np.asarray(desiredListsNDVI[2])
x = NDVIDiff
y = desiredListsNDVI[0]

slope, intercept, r_value, p_value, std_err = stats.linregress(desiredListsNDVI[3], desiredListsNDVI[0])
rSquare = r_value**2
# Z score vs interval scatter plot - NDVI
plt.scatter(x, y, s = 1)
plt.plot(x, intercept + slope * x, '-', label = 'y={:.2f}x+{:.2f}'.format(slope,intercept), color = 'red')
plt.title("dNDVI vs Years Between Disturbances - Fire To Fire (NDVI)")
plt.xlabel("dNDVI")
plt.ylabel("Years Between Disturbances")
plt.legend(fontsize=9)
plt.show()
plt.savefig(r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\fireToFireZScoresNDVI')

# for NBR
x = desiredListsNBR[4]
y = intervalBurnYear

# Z score vs interval scatter plot - NBR
slopeNBR, interceptNBR, r_valueNBR, p_valueNBR, std_errNBR = stats.linregress(x, y)
rSquareNBR = r_valueNBR**2
plt.scatter(x, y, s = .5)
plt.plot(x, interceptNBR + slopeNBR * x, '-', label = 'y={:.2f}x+{:.2f}'.format(slopeNBR,interceptNBR), color = 'red')
plt.title("dNBR vs Years Between Disturbances - Fire To Fire (NBR)")
plt.xlabel("dNBR")
plt.ylabel("Years Between Disturbances")
plt.legend(fontsize=9)
plt.yticks(range(1,18))
plt.show()
plt.savefig(r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\fireToFireZScoresNBR')

# % of pixels recovered against interval between disturbances
recoveredScoresNDVI = np.where(desiredListsNDVI[3]>-1.645, 1, 0)
recoveredIntervalsNDVI = np.multiply(recoveredScoresNDVI, desiredListsNDVI[0])
recoveredScoresNBR = np.where(desiredListsNBR[5]>-1.645, 1, 0)
recoveredIntervalsNBR = np.multiply(recoveredScoresNBR, desiredListsNDVI[0])

# NDVI Part
unique, counts = np.unique(recoveredIntervalsNDVI, return_counts=True)
counts = np.delete(counts, 0)
unique = np.delete(unique, 0)
uniqueIntervals, intCounts = np.unique(desiredListsNDVI[0], return_counts=True)
percentRecoveredDict = dict(zip(unique, counts/intCounts*100))
# NBR Part
uniqueNBR, countsNBR = np.unique(recoveredIntervalsNBR, return_counts=True)
countsNBR = np.delete(countsNBR, 0)
uniqueNBR = np.delete(uniqueNBR, 0)
uniqueIntervalsNBR, intCountsNBR = np.unique(desiredListsNDVI[0], return_counts=True)
percentRecoveredDictNBR = dict(zip(uniqueNBR, countsNBR/intCountsNBR*100))

# positioning
fig, ax = plt.subplots()
barWidth = .35
r1 = np.arange(len(percentRecoveredDict))
list1 = list(percentRecoveredDict.keys()) + list(percentRecoveredDict.keys())
bar1 = plt.bar(r1-barWidth/2, list(percentRecoveredDict.values()), barWidth, label = 'NDVI')
bar2 = plt.bar(r1+barWidth/2, list(percentRecoveredDictNBR.values()), barWidth, label = 'NBR')
plt.xticks(range(0,14), percentRecoveredDict.keys())
plt.xlabel("Interval Between Disturbances (Years)")
plt.ylabel("% of Pixels Recovered")
ax.set_title("Interval Between Disturbances vs % of Pixels Recovered")
plt.legend(prop = {'size':8})
plt.show()
plt.savefig(r'C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\fireToFirePercentRecovered')



