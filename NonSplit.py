import arcpy
import numpy as np
import matplotlib.pyplot as plt
import pickle
from scipy import stats


arcpy.env.overwriteOutput = True
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"

overlaps = arcpy.RasterToNumPyArray("OverlapIntervalFinal")
overlapsOnlyTwo = np.where(overlaps<18, overlaps, 0)
overlapsRaster = arcpy.NumPyArrayToRaster(overlapsOnlyTwo)
overlapsRaster.save("OverlapsFinalOnlyTwo")


# binary versions of the burn rasters
binaryBurnRasters = [a for a in arcpy.ListRasters() if a.startswith("FinalBurn")]  # chooses the correct files from the geodatabase
binaryBurnArrayList = [arcpy.RasterToNumPyArray(x) for x in binaryBurnRasters[1:19]]  # converts them to numpy arrays.  1:19 to leave out 1999 as it isnt needed

binStack = np.dstack(binaryBurnArrayList)  # stack them depth wise so each depth layer is a year
burnAddedArray = np.sum(binStack, axis=2)  # sum up the binary arrays to find the pixels that have burned 2+
onlyTwoBurn = np.where((burnAddedArray < 2), 0, burnAddedArray)  # isolate those pixels, turn the rest into zeros
burnIndices = np.nonzero(onlyTwoBurn)  # get the indices for the nonzero values (the target pixels)

# rasters with years
burnRasters = [a for a in arcpy.ListRasters() if a.startswith("Reclass")]
burnArrayList = [arcpy.RasterToNumPyArray(x) for x in burnRasters[1:19]]
# stack depth wise
yearStack = np.dstack(burnArrayList)
yearStack = yearStack.astype("float")  # change to float so the zeros can be changed to nan
yearStack[yearStack == 0] = 'nan'  # change zeros to not a number so nonzero min can be found easier

# Change workspace to get the NDVI files
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\LandsatClipped.gdb"
NDVIList = [a for a in arcpy.ListRasters() if a.endswith("NDVI")]
NDVIList.sort(key=lambda year: year[14:18])  # sorts the list so that its in temporal order

# change to float
landsatListNDVI = [arcpy.RasterToNumPyArray(a) for a in NDVIList]
for x in landsatListNDVI:
    x = x.astype("float32")

# a dictionary that holds the NDVI years corresponding layer number for indexing NDVI stack
NDVIDict = {}
x = 0
for i in range(1999, 2017):
    NDVIDict[i] = x
    x += 1

# stack the NDVI rasters depth wise
NDVIStack = np.dstack(landsatListNDVI)

# do the same for NBR
NBRList = [a for a in arcpy.ListRasters() if a.endswith("NBR")]
NBRList.sort(key=lambda year: year[14:18])  # sorts the list so that its in temporal order

# NBR rasters to numpy arrays
landsatListNBR = [arcpy.RasterToNumPyArray(a) for a in NBRList]

# a dictionary that holds the NBR years corresponding layer number for indexing NBR stack
NBRDict = {}
x = 0
for i in range(1999, 2017):
    NBRDict[i] = x
    x += 1

# stack the NBR rasters depth wise
NBRStack = np.dstack(landsatListNBR)


# calculate the standard deviation for year before for each year of burnage.
for i in range(2000, 2017):
    stDevList = []
    for x in range(0, 171635):
        if i in yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]:
            stDevList.append([NBRStack[burnIndices[0][x], burnIndices[1][x], NBRDict[i-1]]])
    if len(stDevList) > 0:
        NBRDict["stDev" + str(i)] = np.nanstd(stDevList)

#for NDVI
for i in range(2000, 2017):
    stDevListNDVI = []
    for x in range(0, 171635):
        if i in yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]:
            stDevListNDVI.append([NDVIStack[burnIndices[0][x], burnIndices[1][x], NDVIDict[i-1]]])
    if len(stDevListNDVI) > 0:
        NDVIDict["stDev" + str(i)] = np.nanstd(stDevListNDVI)

# empty lists to store the values to be extracted so they can be plotted easily
intervalBurnYear = []
NDVIBeforeSecondBurn = []
NDVIBeforeFirstBurn = []
NBRBeforeFirstBurn = []
NBRBeforeSecondBurn = []
# stDevList = []
# 171635 is the number of target pixels
for x in range(0, 171635):  # burnIndices is in the form of two lists, one with x coordinates and one with y coordinates
    # so the format of indexing should be yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]), the last index is the
    # depth and 0:19 means a list of values for all layers are returned.
    stDevList.append(NBRDict['stDev'+ str(int((np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]))))])
    #intervalBurnYear.append(float(np.nanmax(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19])-np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19])))
    NBRBeforeSecondBurn.append(float(NBRStack[burnIndices[0][x], burnIndices[1][x], NBRDict[int(np.nanmax(yearStack[burnIndices[0][x], burnIndices[1][x], 0:18]) - 1.0)]]))  # subtract 1 because we want the NDVI/NBR value from the year before the burn
    NBRBeforeFirstBurn.append(float(NBRStack[burnIndices[0][x], burnIndices[1][x], NBRDict[int(np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:18]) - 1.0)]]))

for x in range(0, 171635):  # burnIndices is in the form of two lists, one with x coordinates and one with y coordinates
    # so the format of indexing should be yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]), the last index is the
    # depth and 0:19 means a list of values for all layers are returned.
    # stDevList.append(NDVIDict['stDev'+ str(int((np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19]))))])
    #intervalBurnYear.append(float(np.nanmax(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19])-np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:19])))
    NDVIBeforeSecondBurn.append(float(NDVIStack[burnIndices[0][x], burnIndices[1][x], NDVIDict[int(np.nanmax(yearStack[burnIndices[0][x], burnIndices[1][x], 0:18]) - 1.0)]]))  # subtract 1 because we want the NDVI/NBR value from the year before the burn
    NDVIBeforeFirstBurn.append(float(NDVIStack[burnIndices[0][x], burnIndices[1][x], NDVIDict[int(np.nanmin(yearStack[burnIndices[0][x], burnIndices[1][x], 0:18]) - 1.0)]]))


NBRDiff = np.asarray(NBRBeforeSecondBurn)-np.asarray(NBRBeforeFirstBurn)
NDVIDiff = np.asarray(NDVIBeforeSecondBurn)-np.asarray(NDVIBeforeFirstBurn)
stDevArrayNDVI = np.asarray(stDevListNDVI)
stDevArrayNBR = np.asarray(stDevList)
zScores = np.true_divide(NDVIDiff, stDevArrayNDVI)
zScoresNBR = np.true_divide(NBRDiff, stDevArrayNBR)


# save the lists to a file so the lengthy code doesnt have to be run more than once
desiredLists = [intervalBurnYear, NDVIBeforeFirstBurn, NDVIBeforeSecondBurn, zScores, stDevArrayNDVI, burnIndices]
desiredLists2 = [stDevArrayNBR, NBRBeforeFirstBurn, NBRBeforeSecondBurn, NBRDict, NBRDiff, zScoresNBR]
fname = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\Python\lists599Proj2.pkl" # change accordingly
with open(fname, "wb") as f:
    pickle.dump(desiredLists2, f)


