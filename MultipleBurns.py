import arcpy
import os
from arcpy import env as e
from arcpy.sa import *

arcpy.CheckOutExtension("Spatial")  # check out the needed extension
arcpy.env.overwriteOutput = True
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"
current_dir = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas\NationalBurnsGeoD.gdb"
output_Loc = r"C:\Users\bhegman.stu\Documents\FCOR599\599Project\NationalBurnAreas"

BurnRasters = [a for a in arcpy.ListRasters() if a[0:7] == 'Reclass']
BurnRasters = BurnRasters[-1:] + BurnRasters[:-1]

RasterStr = ""
for i in BurnRasters:
    RasterStr += i + ';'
out_raster = arcpy.sa.Combine(RasterStr)
out_raster.save("BurnOverlapsFinal")

arcpy.CheckInExtension("Spatial")  # check out the needed extension










# for i in BurnRasters:
#     out_raster = arcpy.sa.Reclassify(i, "Value","NODATA 0", "DATA")
#     outRas = os.path.join(current_dir, "Reclass" + i)
#     out_raster.save(outRas)
# for i in reversed(BurnRasters):
#     YearRast = "Reclas_BlankBurn"
#     for n in reversed(BurnRasters):
#         if n == i or BurnRasters.index(n) > BurnRasters.index(i):
#             continue
#         else:
#             BurnInterval = arcpy.sa.Minus(i, n)
#             YearRast = arcpy.sa.Plus(YearRast, BurnInterval)
#     outname = os.path.join(current_dir, "MultiBurn" + i[9:13])
#     YearRast.save(outname)
#
# # Trying with Con Statements
# for i in reversed(BurnRasters[0:6]):
#     CurrentYearInt = int(i[7:11])
#     CurrentYearRaster = arcpy.sa.Raster(i)
#     PrevYear = BurnRasters[BurnRasters.index(i) - 1]
#     PrevYearRaster = Raster(PrevYear)
#     PrevYearInt = int(PrevYear[7:11])
#     BaseRas = arcpy.sa.Con((CurrentYearRaster == CurrentYearInt) & (PrevYearRaster == PrevYearInt), 1, 0)
#     BaseRas.save("BaseRas" + str(CurrentYearInt))
#     for n in reversed(BurnRasters):
#         NYear = int(n[7:11])
#         if BurnRasters.index(n) >= BurnRasters.index(i):
#             continue
#         else:
#             BaseRas = arcpy.sa.Con((CurrentYearRaster == CurrentYearInt) & (Raster(n) == NYear), CurrentYearInt-NYear, "BaseRas" + str(CurrentYearInt))
#             BaseRas.save('BaseRas' + str(CurrentYearInt))
#
#
#
#
#     base_raster = arcpy.sa.RasterCalculator('Con((' + i + ' == ' + i[7:11] + ') & ("Reclass2015"==2015),2,0)')
#     base_raster.save(r"c:\Users\bhegman.stu\documents\FCOR599\599project\nationalburnareas\nationalburnsgeod.gdb\conTEST")
#
#
# output_raster = arcpy.sa.RasterCalculator('Con(("Reclass2017"==2017) & ("Reclass2018"==2018),1,"conTEST")')
# output_raster.save(r"c:\Users\bhegman.stu\documents\FCOR599\599project\nationalburnareas\nationalburnsgeod.gdb\conTEST2")








